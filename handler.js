'use strict';
const AWS = require('aws-sdk');
const Alexa = require("alexa-sdk");
const https = require('https');
const lambda = new AWS.Lambda();
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const uuid = require('uuid');
const appID = '13b7abe2';
const appKey = '7cd191160ec8c7a27717368884a33d49';
const googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyA7MHq2Svdu3cjRoIeiEgzm68JFPAAdypE',
  rate: {limit: 50},
  Promise: Promise
});

exports.handler = function(event, context, callback) {
  const alexa = Alexa.handler(event, context);
  alexa.appId = "amzn1.ask.skill.056dcd8f-9b1f-4e8a-bed2-f615b778ba80";
  alexa.registerHandlers(handlers);
  alexa.execute();
};



// function getTravelAdvice(country) {
//   let url = '/api/content/foreign-travel-advice/'+country.toLowerCase();
//   return new Promise(((resolve, reject) => {
//     var options = {
//         host: 'www.gov.uk',
//         port: 443,
//         path: url,
//         method: 'GET',
//     };
    
//     const request = https.request(options, (response) => {
//       response.setEncoding('utf8');
//       let returnData = '';

//       response.on('data', (chunk) => {
//         returnData += chunk;
//       });

//       response.on('end', () => {
//         console.log(url);
//         console.log(returnData);
//         console.log("BEFORE RESOLVE")
//         resolve(JSON.parse(returnData));
//         console.log("AFTER RESOLVE")
//       });

//       response.on('error', (error) => {
//         console.log("IN ERROR "+ error)
//         reject(error);
//       });
//     });
//     request.end();
//   }));
// }

const handlers = {
  'LaunchRequest': function () {
    this.emit('Prompt');
  },
  'Unhandled': function () {
    this.emit('AMAZON.HelpIntent');
  },
  'GetDurationIntent': function () {
        const ori = this.event.request.intent.slots.origin.value;
       const dest = this.event.request.intent.slots.destination.value;
       if((typeof(ori) != "undefined") || (typeof(dest) != "undefined")){
        this.emit(':tell', 'Please say an origin and destination')
      }
      else{
       console.log(ori + dest)
       let dis;
       let duration;
       let durationTraffic;
       
       const dateInteger = Date.parse(new Date());
       
       const NodeDistanceMatrix = require('node-distance-matrix');
        return await NodeDistanceMatrix.getDistanceMatrixWithTraffic('AIzaSyB79gofiu00SIu7VkT3m-3lVNBo3NAGrmM', ori+' , UK', dest+ ', UK', dateInteger)
        .then((res) => {
            console.log(res);
        return res;
        })
        .then((res) =>{
        duration = res.data.rows[0].elements[0].duration.text
        durationTraffic = res.data.rows[0].elements[0].duration_in_traffic.text
        dis = res.data.rows[0].elements[0].distance.text
        const speechText = 'The distance from ' + ori + ' to ' + dest + ' airport is ' + dis + ' and will take you ' + duration + ' .With the current traffic conditions, it will take you ' + durationTraffic;
        this.emit(':tell', speechText);
        })
      }
  },
  'AMAZON.YesIntent': function () {
    this.emit('Prompt');
  },
  'AMAZON.NoIntent': function () {
    this.emit('AMAZON.StopIntent');
  },
  'Prompt': function () {
    this.emit(':ask', 'Hi there and welcome to Time to airport tracker. Let me know how i can help?', 'Please say that again?');
  },
  'InfoPrompt': function () {
    this.emit(':ask', `Please tell me your origin and destination?`, 'Please say that again?');
  },
  'NoMatch': function () {
    this.emit(':ask', 'Sorry, I could not understand.', 'Please say that again?');
  },
  'AMAZON.HelpIntent': function () {
    const speechOutput = 'Sorry I could not understand that. Please repeat.';
    const reprompt = 'Say hello, to hear me speak.';

    this.response.speak(speechOutput).listen(reprompt);
    this.emit(':responseReady');
  },
  'AMAZON.CancelIntent': function () {
    this.response.speak('Goodbye!');
    this.emit(':responseReady');
  },
  'AMAZON.StopIntent': function () {
    this.response.speak('See you later!');
    this.emit(':responseReady');
  }
};

